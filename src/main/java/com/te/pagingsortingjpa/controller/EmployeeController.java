package com.te.pagingsortingjpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.te.pagingsortingjpa.entity.Employee;
import com.te.pagingsortingjpa.service.EmployeeService;

@RestController
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;

	@GetMapping("/find-all-employee")
	public List<Employee> findAllEmployee() {
		return employeeService.findAllEmployee();
	}

	@GetMapping("/findEmployeeWithSort/{field}")
	public List<Employee> findEmployeeWithSort(@PathVariable String field) {
		return employeeService.findEmployeeWithSort(field);

	}

	@GetMapping("/findEmployeeWithPagination/{offset}/{pageSize}")
	public Page<Employee> findEmployeeWithPagination(@PathVariable int offset, @PathVariable int pageSize) {
		Page<Employee> employee = employeeService.findEmployeeWithPagination(offset, pageSize);
		return employee;
	}

}
