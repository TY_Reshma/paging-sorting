package com.te.pagingsortingjpa.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.te.pagingsortingjpa.entity.Employee;
import com.te.pagingsortingjpa.repository.EmployeeRepository;

@Service
public class EmployeeService {
	@Autowired
	private EmployeeRepository employeeRepository;
	
	int pageSize = 5;
	int pageNumber = 1;

	public List<Employee> findAllEmployee() {
		return employeeRepository.findAll();
	}

	public List<Employee> findEmployeeWithSort(String field) {
		return employeeRepository.findAll(Sort.by(Sort.Direction.ASC, field));

	}

	@SuppressWarnings("unused")
	public Page<Employee> findEmployeeWithPagination(int offset, int pageSize) {
		List<Page> arrayList = new ArrayList<>();
		for (Page page : arrayList) {
			Page<Employee> employee = employeeRepository.findAll(PageRequest.of(offset, pageSize));
			return employee;
		}
		return employeeRepository.findAll(PageRequest.of(offset, pageSize));
	}

}
