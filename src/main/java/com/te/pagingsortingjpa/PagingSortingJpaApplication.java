package com.te.pagingsortingjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PagingSortingJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagingSortingJpaApplication.class, args);
	}

}
